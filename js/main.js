document.addEventListener("DOMContentLoaded", () => {
  let listWords=["cooperatiu","curios","pacient",];
  

  let guessedWords = [[]];
  let availableSpace = 1;
  let endGame=false;

  let word;
  let lenWord;
  getNewWord();
  let guessedWordCount = 0;
  createSquares();
  const keys = document.querySelectorAll(".keyboard-row button");

  function getNewWord() {
    let now = new Date();
    let start = new Date(now.getFullYear(), 0, 0);
    let diff = (now - start) + ((start.getTimezoneOffset() - now.getTimezoneOffset()) * 60 * 1000);
    let oneDay = 1000 * 60 * 60 * 24;
    let day = Math.floor(diff / oneDay);
    let wordPos=day%listWords.length;
    word = listWords[wordPos]; 
    lenWord=word.length;
    console.log(word,wordPos,day);
  }

  function getCurrentWordArr() {
    const numberOfGuessedWords = guessedWords.length;
    return guessedWords[numberOfGuessedWords - 1];
  }

  function updateGuessedWords(letter) {
    const currentWordArr = getCurrentWordArr();

    if (currentWordArr && currentWordArr.length < lenWord) {
      currentWordArr.push(letter);

      const availableSpaceEl = document.getElementById(String(availableSpace));

      availableSpace = availableSpace + 1;
      availableSpaceEl.textContent = letter;
    }
  }

  function getTileColor(letter, index) {
    const isCorrectLetter = word.includes(letter);

    if (!isCorrectLetter) {
      return "rgb(58, 58, 60)";
    }

    const letterInThatPosition = word.charAt(index);
    const isCorrectPosition = letter === letterInThatPosition;

    if (isCorrectPosition) {
      return "rgb(83, 141, 78)";
    }

    return "rgb(181, 159, 59)";
  }

  function handleSubmitWord() {
    if (endGame == true){
      messages("Final del joc ");
    }else{
      const currentWordArr = getCurrentWordArr();
      if (currentWordArr.length !== lenWord) {
        let textMessage="La paraula ha de tenir "+lenWord+" lletres";
        //window.alert("Word must be 5 letters");
        messages(textMessage);
        return;
      }

      const currentWord = currentWordArr.join("");
      const firstLetterId = guessedWordCount * lenWord + 1;
      const interval = 200;
          currentWordArr.forEach((letter, index) => {
            setTimeout(() => {
              const tileColor = getTileColor(letter, index);

              const letterId = firstLetterId + index;
              const letterEl = document.getElementById(letterId);
              letterEl.classList.add("animate__flipInX");
              letterEl.style = `background-color:${tileColor};border-color:${tileColor}`;
            }, interval * index);
          });

          guessedWordCount += 1;

          if (currentWord === word) {
            //messages("Congratulations!");
            window.alert("Felicitats!");
            endGame=true;
            return;
          }

          if (guessedWords.length === 6) {
            window.alert(`Ho sento, no tens més conjectures! La paraula és ${word}.`);
            endGame=true;
            return;
          }

          guessedWords.push([]);
        
          //window.alert("Word is not recognised!");
    }
  }

  function createSquares() {
    const gameBoard = document.getElementById("board");
    gameBoard.style.gridTemplateColumns ="repeat("+lenWord+", 1fr)";
    for (let index = 0; index < 6*lenWord; index++) {
      let square = document.createElement("div");
      square.classList.add("square");
      square.classList.add("animate__animated");
      square.setAttribute("id", index + 1);
      gameBoard.appendChild(square);
    }
  }

  function handleDeleteLetter() {
    const currentWordArr = getCurrentWordArr();
    const removedLetter = currentWordArr.pop();

    guessedWords[guessedWords.length - 1] = currentWordArr;

    const lastLetterEl = document.getElementById(String(availableSpace - 1));

    lastLetterEl.textContent = "";
    availableSpace = availableSpace - 1;
  }
  
  function messages(text) {
  // Get the snackbar DIV
  let x = document.getElementById("snackbar");
  let snackbartext=document.getElementById("snackbartext");
  snackbartext.textContent=text;

  // Add the "show" class to DIV
  x.className = "show";

  // After 3 seconds, remove the show class from DIV
  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
  } 
  

  for (let i = 0; i < keys.length; i++) {
    keys[i].onclick = ({ target }) => {
      const letter = target.getAttribute("data-key");

      if (letter === "enter" && endGame === false) {
        handleSubmitWord();
        return;
      }

      if (letter === "del" && endGame === false) {
        handleDeleteLetter();
        return;
      }

      updateGuessedWords(letter);
    };
  }
});






  // When the user clicks on the button, open the modal
function showInfo() {
     let modal = document.getElementById("information");
     if (modal.style.display == "block"){
       modal.style.display = "none";
     }else{
       modal.style.display = "block";
     }
}



  // When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    let modal = document.getElementById("information");
    if (event.target == modal) {
      
      modal.style.display = "none";
    }
} 

